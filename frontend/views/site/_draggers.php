<?php

use common\models\Option;
use common\components\helpers\StringHelper;
use common\models\ApplicationForm;
//use common\components\Formulae;

?>

<div class="calculator clearfix" id="slide-1">
    <?php
    $interest_per_month = Option::get('draggers_interest_per_month');
    $interest_per_month_annuity = Option::get('draggers_interest_per_month_annuity');
    $annuity_overpay_fraction = 0.0239;// Хардкод, без расчета Formulae::getMinOverpayFraction();

    $sum_from = Option::get('draggers_sum_from');
    $sum_to = Option::get('draggers_sum_to');
    $sum_step = Option::get('draggers_sum_step');
    $sum_default = Option::get('draggers_sum_default');

    $term_from = Option::get('draggers_term_from');
    $term_to = Option::get('draggers_term_to');
//    $term_step = Option::get('draggers_term_step');
    $term_default = Option::get('draggers_term_default');
    ?>
    <input type="hidden" name="draggers[interest_per_month]" value="<?= $interest_per_month ?>"/>
    <input type="hidden" name="draggers[interest_per_month_annuity]" value="<?= $interest_per_month_annuity ?>"/>

    <input type="hidden" name="draggers[sum_from]" value="<?= $sum_from ?>"/>
    <input type="hidden" name="draggers[sum_to]" value="<?= $sum_to ?>"/>
    <input type="hidden" name="draggers[sum_step]" value="<?= $sum_step ?>"/>
    <input type="hidden" name="draggers[sum_default]" value="<?= $sum_default ?>"/>

    <input type="hidden" name="draggers[term_from]" value="<?= $term_from ?>"/>
    <input type="hidden" name="draggers[term_to]" value="<?= $term_to ?>"/>
    <?php /*<input type="hidden" name="draggers[term_step]" value="<?= $term_step ?>"/> */?>
    <input type="hidden" name="draggers[term_default]" value="<?= $term_default ?>"/>

    <div class="col-sm-6">
        <div class="calculator-block clearfix">
            <div class="clearfix">
                <div class="title">Сумма займа:&nbsp;</div>
                <div class="price">
                    <div class="slider-result" id="sum-dragger-value">
                        <div class="result-sum clearfix">
                            <input
                                id="sum-dragger-input"
                                class="sum-dragger-input"
                                type="text"
                                name="sum-field"
                                value="<?= $sum_default ?>"
                            />
                            <span class="rub">a</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="from"><?= number_format($sum_from, 0, ',', ' ') ?><span class="rub">a</span></div>
            <div class="till"><?= number_format($sum_to, 0, ',', ' ') ?><span class="rub">a</span></div>

            <div class="polzun clearfix">
                <div class="slider dragdealer" id="sum-dragger">
                    <div class="handle slider-handle"></div>
                </div>
            </div>
        </div>
        <div class="calculator-block clearfix">
            <?php $months_forms = ['месяц', 'месяца', 'месяцев']; ?>
            <div class="clearfix">
                <div class="title">На какой срок:</div>
                <?php /*<div class="month">
                    <div class="slider-result" id="term-dragger-value">
                        <?= number_format($term_default, 0, ',', ' ') ?>
                    </div>
                    &nbsp;<span id="months-word"><?= StringHelper::pluralize($term_default, $months_forms) ?></span>
                </div>*/ ?>
            </div>
            <div class="month term-radio">
                <div class="slider-result term-radio-value">
                    <?= number_format($term_from, 0, ',', ' ') ?>
                </div>
                &nbsp;<span><?= StringHelper::pluralize($term_from, $months_forms) ?></span>
            </div>
            <div class="month term-radio">
                <div class="slider-result term-radio-value">
                    <?= number_format($term_to, 0, ',', ' ') ?>
                </div>
                &nbsp;<span><?= StringHelper::pluralize($term_to, $months_forms) ?></span>
            </div>
            <?php /*
            <div class="from">
                <?= number_format($term_from, 0, ',', ' ') ?>&nbsp;
                <?= StringHelper::pluralize($term_from, $months_forms) ?>
            </div>
            <div class="till">
                <?= number_format($term_to, 0, ',', ' ') ?>&nbsp;
                <?= StringHelper::pluralize($term_to, $months_forms) ?>
            </div>
            <div class="polzun clearfix">
                <div class="slider dragdealer" id="term-dragger">
                    <div class="handle slider-handle"></div>
                </div>
            </div>
 */ ?>
        </div>
        <p>Переплата по займу от <?= number_format($annuity_overpay_fraction * 100, 2, ',', ' ') ?>% в месяц *.</p>
        <div class="backcall-holder">
            <div class="backcall button_2"
                 onclick="yaCounter30721508.reachGoal('zakazat-zvonok-click');ga('send', 'event', 'Knopka', 'zakazat-zvonok-click');">
                Заказать обратный звонок
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="platej clearfix">
            <?php $can_apply = Option::get('draggers_service_active'); ?>
            <div
                class="platej-block top clearfix js-payments-variant active<?= $can_apply ? '' : ' platej-block-static' ?>"
                data-value="<?= ApplicationForm::PAYMENTS_PERCENT ?>">
                <div class="text">
                    Ежемесячное погашение только процентов, а сумма основного долга выплачивается в конце срока <br>
                    Процентная ставка <b><?= $interest_per_month * 100 ?>% в месяц</b>
                </div>
                <div class="platej-month">Платёж в месяц:</div>
                <div class="price"><span id="percent-per-month-value"></span><span class="rub">a</span></div>
            </div>
            <div
                class="platej-block bottom clearfix js-payments-variant<?= $can_apply ? '' : ' platej-block-static' ?>"
                data-value="<?= ApplicationForm::PAYMENTS_ANNUITY ?>">
                <div class="text">
                    Ежемесячное погашение равными частями основного долга, и процентов<br>
                    Переплата от <b><?= number_format($annuity_overpay_fraction * 100, 2, ',', ' ') ?>% в месяц</b>
                </div>
                <div class="platej-month">Платёж в месяц:</div>
                <div class="price"><span id="annuity-per-month-value"></span><span class="rub">a</span></div>
            </div>
            <?php if ($can_apply) { ?>
                <div class="zayavka-holder">
                    <div class="zayavka clearfix" onclick="yaCounter30721508.reachGoal('zaivka-click'); ga('send', 'event', 'Knopka', 'zaivka-click');">
                        Оформить заявку
                    </div>
                </div>
            <?php } ?>
            <input type="hidden" name="application-enabled" value="<?= $can_apply ? 1 : 0 ?>"/>
        </div>
    </div>
    <span class="col-sm-12 mini">
        Данный расчет является ориентировочным, носит информационный характер и не является офертой по выдаче Вам займа
        в указанной сумме. Окончательный расчет будет произведен на основе Вашей заявки специалистами
        Общество с ограниченной ответственностью &laquo;МИКРОКРЕДИТНАЯ КОМПАНИЯ АСД-ФИНАНС&raquo;.
    </span>
</div>
