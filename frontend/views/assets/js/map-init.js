ymaps.ready(function () {

    if ($('#index-page-map').length == 0) {
        return;
    }

    icon_layout = ymaps.templateLayoutFactory.createClass(
        '<div class="map-icon">*</div>'
    );

    balloon_layout = ymaps.templateLayoutFactory.createClass(
        '<div class="map-balloon">' +
            '<a class="close-button" href="#">&times;</a>' +
            '<span class="text">$[properties.text]</span>' +
            '</div>',
        {
            build: function () {
                this.constructor.superclass.build.call(this);

                this.$element = $(this.getParentElement()).find('.map-balloon');

                this.$element
                    .find('.close-button')
                    .on('click', $.proxy(this._onCloseButtonClick, this));
            },
            clear: function () {
                this.$element
                    .find('.close-button')
                    .off('click');

                this.constructor.superclass.clear.call(this);
            },
            _onCloseButtonClick: function (e) {
                e.preventDefault();
                this.events.fire("userclose");
            }
        }
    );

    placemark_options = {
        iconLayout: icon_layout,
        iconOffset: [-22, -53],
        balloonLayout: balloon_layout,
        balloonOffset: [-21, -75],
        balloonShadow: false
    };

    function MapController(map_data) {

        this.map = new ymaps.Map("index-page-map", {
            center: map_data['center'],
            zoom: map_data['zoom']
        });
		searchControl = new ymaps.control.SearchControl({ provider: 'yandex#publicMap' });

		this.map.controls.add('zoomControl', { top: 75, left: 5 });
		
		//var trafficControl = new ymaps.control.TrafficControl({shown: true});
		//this.map.controls.add(trafficControl, {top: 10, left: 10});
		
        this.placemarks = [];
        for (var i = 0; i < map_data['placemarks'].length; ++i) {
            var placemark_data = map_data['placemarks'][i];
            // Делаем метку кликабельно, только если указан адрес офиса
            placemark_options.hasBalloon = placemark_data['title'].length > 0;
            var placemark_instance = new ymaps.Placemark(
                placemark_data['coords'],
                { text: placemark_data['title'], number: placemark_data['number'] },
                placemark_options
            );

            this.map.geoObjects.add(placemark_instance);
            this.placemarks.push(placemark_instance);
        }

        $('.js-map-link').each($.proxy(function (event, element) {
            var $element = $(element);
            var x = $element.attr('data-x');
            var y = $element.attr('data-y');
            var index = $element.attr('data-placemark-index');
            $element.click(_.throttle($.proxy(function (event) {
                event.preventDefault();

                this.map.setCenter([x, y], map_data['zoom'], {
                    duration : 300,
                    callback : $.proxy(function () {
                        this.placemarks[index].balloon.open();
                    }, this)
                });
            }, this), 400));
        }, this));

    }

    var $map_data_input = $('input[name="map-data"]');
    if ($map_data_input.length == 0) {
        throw 'Не найдены данные карты';
    }
    var map_data = JSON.parse($map_data_input.val());

    //todo
    // map_data={"center":[47.2039478,39.6769785],"zoom":15,"placemarks":[{"coords":[47.2039478,39.6769785],"number":0,"title":"344034, г. Ростов-на-Дону,<br/>ул. Портовая, 251, оф. 5"}]};

    map_controller = new MapController(map_data);

});