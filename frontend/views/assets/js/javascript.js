var $ = jQuery.noConflict();

$(function () {
    $('.backcall').click(function () {
        $('.overpage').css('display', 'block');
        setTimeout(function () {
            $('.overpage').css('opacity', '1');
        }, 100)
    });

    $('.close').click(function () {
        $('.overpage').css('opacity', '0');
        setTimeout(function () {
            $('.overpage').css('display', 'none');
            $('.call').css('height', '370px');
            $('.callform').css('display', 'block');
            $('.calltitle').text('Заказ обратного звонка');
        }, 1000)
    });

    $('.zayavka').click(function () {
        $('.overpage_z').css('display', 'block');
        setTimeout(function () {
            $('.overpage_z').css('opacity', '1');
        }, 100)
    });

    $('.close_z').click(function () {
        $('.overpage_z').css('opacity', '0');
        setTimeout(function () {
            $('.overpage_z').css('display', 'none');
            $('.call_z').css('height', '658px');
            $('.callform_z').css('display', 'block');
            $('.calltitle_z').text('Оформить заявку');
        }, 1000)
    });
});
//плавная прокрутка
function anchorScroller(el, duration) {
    if (this.criticalSection) {
        return false;
    }

    if ((typeof el != 'object') || (typeof el.href != 'string'))
        return true;

    var address = el.href.split('#');
    if (address.length < 2)
        return true;

    address = address[address.length - 1];
    el = 0;

    for (var i = 0; i < document.anchors.length; i++) {
        if (document.anchors[i].name == address) {
            el = document.anchors[i];
            break;
        }
    }
    if (el === 0)
        return true;

    this.stopX = 0;
    this.stopY = 0;
    do {
        this.stopX += el.offsetLeft;
        this.stopY += el.offsetTop;
    } while (el = el.offsetParent);

    this.startX = document.documentElement.scrollLeft || window.pageXOffset || document.body.scrollLeft;
    this.startY = document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop;

    this.stopX = this.stopX - this.startX;
    this.stopY = this.stopY - this.startY;

    if ((this.stopX == 0) && (this.stopY == 0))
        return false;

    this.criticalSection = true;
    if (typeof duration == 'undefined')
        this.duration = 300;
    else
        this.duration = duration;

    var date = new Date();
    this.start = date.getTime();
    this.timer = setInterval(function () {
        var date = new Date();
        var X = (date.getTime() - this.start) / this.duration;
        if (X > 1)
            X = 1;
        var Y = ((-Math.cos(X * Math.PI) / 2) + 0.5);

        cX = Math.round(this.startX + this.stopX * Y);
        cY = Math.round(this.startY + this.stopY * Y);

        document.documentElement.scrollLeft = cX;
        document.documentElement.scrollTop = cY;
        document.body.scrollLeft = cX;
        document.body.scrollTop = cY;

        if (X == 1) {
            clearInterval(this.timer);
            this.criticalSection = false;
        }
    }, 10);
    return false;
}

var br = $.browser;
$(window).scroll(function () {
    var top = $(document).scrollTop();
    if (top < 460) {
        $('.navbar-collapse.collapse').css({top: '0px', position: 'relative'});
        $('.navbar-header').css({top: '0px', position: 'relative'});
        $('.navbar-collapse.in').css({top: '0px', position: 'relative'});
    }
    else {
        $('.navbar-collapse.collapse').css({top: '0px', position: 'fixed', width: '100%'});
        $('.navbar-header').css({top: '0px', position: 'fixed', width: '100%'});
        $('.navbar-collapse.in').css({top: '40px', position: 'fixed', width: '100%'});
    }
});

$(function () {
    var wrapper = $(".file_upload"),
        inp = wrapper.find("input"),
        btn = wrapper.find(".button"),
        lbl = wrapper.find("mark");

    // Crutches for the :focus style:
    inp.focus(function () {
        wrapper.addClass("focus");
    }).blur(function () {
        wrapper.removeClass("focus");
    });

    var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;

    inp.change(function () {
        var file_name;
        if (file_api && inp[ 0 ].files[ 0 ])
            file_name = inp[ 0 ].files[ 0 ].name;
        else
            file_name = inp.val().replace("C:\\fakepath\\", '');

        if (!file_name.length)
            return;

        if (lbl.is(":visible")) {
            lbl.text(file_name);
            btn.text("Выбрать");
        } else
            btn.text(file_name);
    }).change();

});
$(window).resize(function () {
    $(".file_upload input").triggerHandler("change");
});

/*$(document).ready(function(){
 $( ".slider" ).slider({
 animate: true,
 range: "min",
 value: 5000000,
 min: 300000,
 max: 10000000,
 step: 1,

 slide: function( event, ui ) {
 $( "#slider-result" ).html(ui.value);
 },
 change: function(event, ui) {
 $('#text').attr('value', ui.value);
 }
 });
 });*/

$(function () {
    var $application_enabled = $('input[name="application-enabled"]');
    if ($application_enabled.length == 0 || $application_enabled.val() == 0) {
        return;
    }

    var $payment_variants = $('.js-payments-variant');
    var $payments_type_form_radios = $('.overpage_z input[name="ApplicationForm[payments_type]"]');
    $payment_variants.click(function () {
        $payment_variants.removeClass('active');
        var $this = $(this);
        var val = $this.attr('data-value');
        $payments_type_form_radios.filter('*[value="'+val+'"]').prop('checked', true);
        $.uniform.update($payments_type_form_radios);
        $this.addClass('active');
    });

    $payments_type_form_radios.change(function () {
        $payment_variants.removeClass('active');
        var val = $(this).val();
        $payment_variants.filter('*[data-value="'+val+'"]').addClass('active');
    });

    $('input[type="radio"]').uniform();
});

$(function () {
    //Определение модуля и его вызов отделены, так как можно изменить его пере вызовом.
    //Используется для переопределения методов в других модулях
    loanDraggersModule.run();
});