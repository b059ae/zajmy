$(function () {

    //Любые числа должны быть предварительно приведены от строк к числам!
    function Dragger(dom_id, left, right, step, default_value) {
        this.left = left;
        this.right = right;
        this.step = step;
        this.callback = function (val) {};

        this.length = right - left;
        this.intervals_count = this.length / step;
        this.positions = this.intervals_count + 1;

        this.xToVal = function (x) {
            return Math.round(this.left + this.length * x);
        };
        this.valToX = function (val) {
            var x = (val - this.left)/this.length;
            if (x > 1) {
                x = 1;
            }
            if (x < 0) {
                x = 0;
            }

            return x;
        };

        this.instance = new Dragdealer(dom_id, {
            //Диапазон движения бегунка раздвинут на 10px влево и вправо, чтобы визуальные части точно попадали
            left: -1,
            right: -1,
            steps: this.positions,
            x: this.valToX(default_value),
            snap: true,
            animationCallback: $.proxy(function (x, y) {
                this.callback(this.xToVal(x));
            }, this)
        });

        this.getVal = function () {
            var xy = this.instance.getValue();
            return this.xToVal(xy[0]);
        };

        this.setVal = function (val) {
            this.instance.setValue(this.valToX(val), 0, true);
        };
    }

    function Radio(dom_id, dom_value_id, default_value){
        this.val = default_value;

        $("."+dom_id).click({th: this},function(e){
            var val = $(this).find("."+dom_value_id).text();
            e.data.th.val = val;
            e.data.th.callback(val);
        });

        this.callback = function (val) {};

        this.getVal = function () {
            return this.val;
        };
    }

    //Здесь модуль только определяется, запускать надо с другого файла. Его подключают и запускают в разных местах
    loanDraggersModule = {

        initModule: function () {
            this.data = {
                interest_per_month: this.getFromDom('interest_per_month'),
                interest_per_month_annuity: this.getFromDom('interest_per_month_annuity'),

                sum_from: this.getFromDom('sum_from'),
                sum_to: this.getFromDom('sum_to'),
                sum_step: this.getFromDom('sum_step'),
                sum_default: this.getFromDom('sum_default'),

                term_default: this.getFromDom('term_default')
            };

            this.dom_ids = {
                sum_dragger: 'sum-dragger',
                term_radio: 'term-radio',
                term_radio_value: 'term-radio-value',
                sum_dragger_input: 'sum-dragger-input',
                percent_per_month_value: 'percent-per-month-value',
                annuity_per_month_value: 'annuity-per-month-value',
                months_word: 'months-word'
            };

            this.formulae = {
                percentPerMonth: $.proxy(function (sum, months) {
                    return sum * this.data.interest_per_month;
                }, this),
                annuityPerMonth: $.proxy(function (sum, months) {
                    return sum * this.data.interest_per_month_annuity * ( 1 + 1 / (Math.pow(1 + this.data.interest_per_month_annuity, months) - 1) );
                }, this)
            };
        },

        getFromDom: function (param) {
            var $input = $('input[name="draggers['+param+']"]');
            if ($input.length == 0) {
                throw new Error(param + ' - не найдено поле для инициализации');
            }
            //Функция Number приводит числа из дома к числам от строк. Всё в модуле считается в числах.
            return Number($input.val());
        },

        initPluginsAndBindCallbacks: function () {
            this.$percent_per_month_value = $('#' + this.dom_ids.percent_per_month_value);
            this.$annuity_per_month_value = $('#' + this.dom_ids.annuity_per_month_value);

            this.$sum_dragger_input = $('#' + this.dom_ids.sum_dragger_input);
            this.$term_radio_value = $('.' + this.dom_ids.term_radio_value);
            this.$months_word = $('#' + this.dom_ids.months_word);

            this.$sum_dragger_input.autoNumeric('init', {
                aSep: ' ',
                dGroup: 3,
                aDec: ',',
                mDec: 0,
                vMin: 0
            });

            var sum_dragger = new Dragger(
                this.dom_ids.sum_dragger,
                this.data.sum_from,
                this.data.sum_to,
                this.data.sum_step,
                this.data.sum_default
            );

            term_radio = new Radio(
                this.dom_ids.term_radio,
                this.dom_ids.term_radio_value,
                this.data.term_default
            );

            sum_dragger.callback = $.proxy(function (val) {
                this.recalculate(val, term_radio.getVal());
                this.renderSum(val);
            }, this);

            term_radio.callback = $.proxy(function (val) {
                this.recalculate(sum_dragger.getVal(), val);
                this.renderTerm(val);
            }, this);

            this.$sum_dragger_input.blur(_.debounce($.proxy(function() {
                sum_dragger.setVal(this.$sum_dragger_input.autoNumeric('get'));
            }, this), 300));

            this.recalculate(sum_dragger.getVal(), term_radio.getVal());
            this.renderSum(sum_dragger.getVal());
            this.renderTerm(term_radio.getVal());
        },

        recalculate: function(sum, months) {
            this.$percent_per_month_value.html( niceNumber(this.formulae.percentPerMonth(sum, months)) );
            this.$annuity_per_month_value.html( niceNumber(this.formulae.annuityPerMonth(sum, months)) );
        },

        renderSum: function (val) {
            this.$sum_dragger_input.autoNumeric('set', val);
        },
        renderTerm: function (val) {
            this.$term_radio_value.parent().removeClass('active');
            this.$term_radio_value.filter(":contains('"+val+"')").parent().addClass('active');
        },

        pluralizeMonths: function (number) {
            if (number >= 11 && number <= 19) {
                return 'месяцев';
            }

            var digit = Number(number.toString().slice(-1));
            if (digit == 0 || digit > 4) {
                return 'месяцев';
            }
            if (digit >= 2 && digit <= 4) {
                return 'месяца';
            }
            return 'месяц';
        },

        run: function () {
            this.initModule();
            this.initPluginsAndBindCallbacks();
        }

    };

});
