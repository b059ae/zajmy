<?php

use common\models\CallOrderForm;
use common\models\Option;
use frontend\assets\AppAsset;
use common\models\ApplicationForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use frontend\widgets\regions_select\RegionsSelect;
//se common\components\Formulae;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$subdomain = Yii::$app->subdomains->getCurrent();

$this->title = $this->title ?? $this->params['title'] ?? $subdomain->seo_title;
$this->params['seo_description'] = $this->params['seo_description'] ?? $subdomain->seo_desc;
$this->params['seo_keywords'] = $this->params['seo_keywords'] ?? 'Займы под залог недвижимости, деньги под залог недвижимости, заложить недвижимость в Ростове-на-Дону, ссуда под залог недвижимости, кредит под залог недвижимости';

$is_subdomain = count(explode('.', $_SERVER['HTTP_HOST'])) > 2;

$header = $this->params['header'] ?? 'Займы под залог недвижимости';
$is_home = $this->params['is_home'] ?? false;
?>
<?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= Html::encode($this->title); ?></title>

        <?php if (!empty($this->params['seo_description'])) : ?>
            <meta name="description" content="<?= $this->params['seo_description']; ?>">
        <?php endif; ?>

        <?php if (!empty($this->params['seo_keywords'])) : ?>
            <meta name="keywords" content="<?= $this->params['seo_keywords']; ?>">
        <?php endif; ?>

        <meta name="format-detection" content="telephone=no">
        <meta name='yandex-verification' content='5619d78c10d44998'/>
        <meta name="google-site-verification" content="vXK-KuuEVNHXanS2SMUIgBBHOY1absnk0P0qhh6GhbI"/>

        <meta property="og:title" content="Займы под залог недвижимости"/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="https://zajmy.su"/>
        <meta property="og:image" content="https://zajmy.su/images/headerbg.jpg"/>
        <meta property="og:locale" content="ru-RU"/>
        <meta property="og:description"
              content="Компания «АСД-ФИНАНС» предоставляет денежные займы под залог недвижимости на Юге России. Сумма займа начинается от 300 тысяч и достигает 10 миллионов рублей (под низкие проценты). Займы оформляются по договору обременения, который регистрируется в органах юстиции."/>

        <?= Html::csrfMetaTags(); ?>

        <?php $this->head(); ?>

        <link rel="shortcut icon" href="/favicon.ico?v=2" type="image/x-icon"/>

        <!--[if lt IE 9]>
        <script src="../../assets/js/ie8-responsive-file-warning.js"></script>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script type="text/javascript">
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    },
                    i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })
            (window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-78987997-1', 'auto');
            ga('send', 'pageview');
        </script>
    </head>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter30721508 = new Ya.Metrika({
                        id: 30721508,
                        webvisor: true,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true
                    });
                } catch (e) {
                }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/30721508" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->

    <body>

    <input type="hidden" name="send_call_order_url" value="<?= Url::to(['/site/order-call']); ?>"/>
    <input type="hidden" name="apply_url" value="<?= Url::to(['/site/apply']); ?>"/>

    <div class="overpage">
        <div class="call">
            <div class="close">X</div>
            <div class="calltitle">Заказ обратного звонка</div>
            <div class="callform">
                <?php
                $call_order = new CallOrderForm();

                $call_order_form = ActiveForm::begin([
                    'action' => Url::to(['/site/order-call']),
                    'options' => ['class' => 'backcalaasl'],
                    'enableClientValidation' => false,
                ]);

                echo $call_order_form
                    ->field($call_order, 'name')
                    ->label(false)
                    ->textInput([
                        'class' => 'formname formfield minformfield',
                        'placeholder' => 'Ваше имя',
                    ]);

                echo $call_order_form
                    ->field($call_order, 'phone')
                    ->label(false)
                    ->widget(MaskedInput::className(), [
                        'class' => 'formfield minformfield',
                        'mask' => '+7 (999) 999-99-99',
                        'options' => [
                            'placeholder' => 'Ваш телефон',
                        ],
                        'clientOptions' => [
                            'showMaskOnHover' => false,
                        ],
                    ]);
                ?>
                <div class="text-center">
                    <input type="button" name="send" class="buttonsendfcall" value="ОТПРАВИТЬ"
                           onclick="yaCounter30721508.reachGoal('zakazat-zvonok-send');ga('send', 'event', 'Knopka', 'zakazat-zvonok-send');">
                </div>
                <?php $call_order_form->end(); ?>
            </div>
        </div>
    </div>

    <div class="overpage_z">
        <div class="call_z">
            <div class="close_z">X</div>
            <div class="calltitle_z">Предварительная заявка на займ</div>
            <div class="callform_z">
                <?php
                $application = new ApplicationForm;
                $application_form = ActiveForm::begin([
                    'action' => Url::to(['/site/apply']),
                    'options' => ['class' => 'backcalaasl_z'],
                    'enableClientValidation' => false,
                ]);
                ?>
                <input
                        type="text"
                        name="<?= Html::getInputName($application, 'name') ?>"
                        placeholder="<?= $application->getAttributeLabel('name') ?>"
                        title="<?= $application->getAttributeLabel('name') ?>"
                        size="1"
                        class="formname formfield minformfield width_big"
                />
                <?= Html::activeDropDownList($application, 'subject_type', ApplicationForm::getSubjectTypesList(), [
                    'class' => 'formfield minformfield width_big',
                    'size' => '1',
                    'title' => $application->getAttributeLabel('subject_type'),
                ]) ?>
                <?= Html::activeRadioList($application, 'payments_type', ApplicationForm::getPaymentsTypesList(), [
                    'unselect' => null,
                    'class' => 'formfield minformfield width_big formfield-radio',
                    'title' => $application->getAttributeLabel('payments_type'),
                ]) ?>
                <input
                        type="text"
                        name="<?= Html::getInputName($application, 'city') ?>"
                        placeholder="<?= $application->getAttributeLabel('city') ?>"
                        title="<?= $application->getAttributeLabel('city') ?>"
                        size="1"
                        class="formfield minformfield width_big"
                />
                <?= MaskedInput::widget([
                    'model' => $application,
                    'attribute' => 'phone',
                    'mask' => '+7 (999) 999-99-99',
                    'options' => [
                        'class' => 'formfield minformfield width_big',
                        'size' => '1',
                        'placeholder' => $application->getAttributeLabel('phone'),
                        'title' => $application->getAttributeLabel('phone'),
                    ],
                ]) ?>
                <input
                        type="text"
                        name="<?= Html::getInputName($application, 'sum') ?>"
                        placeholder="<?= $application->getAttributeLabel('sum') ?>"
                        title="<?= $application->getAttributeLabel('sum') ?>"
                        size="1"
                        class="formfield minformfield width_big"
                />
                <textarea
                        name="<?= Html::getInputName($application, 'description') ?>"
                        placeholder="<?= $application->getAttributeLabel('description') ?>"
                        title="<?= $application->getAttributeLabel('description') ?>"
                        class="formfield minformfield width_big"
                ></textarea>
                <input type="button" name="send" class="buttonsendfcall_z" onclick="yaCounter30721508.reachGoal
                ('zaivka-send');ga('send', 'event', 'Knopka', 'zaivka-send');" value="ОТПРАВИТЬ">
                <?php $application_form->end(); ?>
            </div>
        </div>
    </div>

    <div class="header">

        <?php
        echo RegionsSelect::widget([
            'subdomains' => \Yii::$app->subdomains,
            'view_path' => '@frontend/widgets/regions_select/views/regions-list-mobile',
        ]);
        ?>

        <div class="container">
            <div class="col-sm-6 col-md-8">
                <h1 class="title"><?= $header ?><br/>
                    <?= ($subdomain->prepositional_case && $is_subdomain) ? ' в ' . $subdomain->prepositional_case : '' ?>
                </h1>
                <p class="description">
                    <?php
                    $annuity_overpay_fraction = 0.0239;// Хардкод, без расчета Formulae::getMinOverpayFraction();
                    ?>
                    Переплата по займу от <?= number_format($annuity_overpay_fraction * 100, 2, ',', ' ') ?>%
                    в
                    месяц *.<br/>
                    Принятие решения за 1 день.<br/>
                    Без подтверждения дохода, кредитная история не играет роли.
                </p>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="topcont pull-right">
                    <div class="phone">
                        <span><a href="tel:88005504109">8 (800) 550 41 09</a></span>
                        <br/>
                        Звонок по России бесплатный<br/>
                        <div class="time">С 6<sup>00</sup> до 21<sup>00</sup></div>
                    </div>

                    <?= RegionsSelect::widget(['subdomains' => \Yii::$app->subdomains]); ?>

                    <div class="address">
                        <?php if ($subdomain->params['address']) : ?>
                            Наш адрес:<br/>
                            <?= $subdomain->params['address']; ?>
                        <?php endif; ?>
                        Email: <a href="mailto:info@zajmy.su">info@zajmy.su</a>
                    </div>
                    <div class="backcall button_1"
                         onclick="yaCounter30721508.reachGoal('zakazat-zvonok-click'); ga('send', 'event', 'Knopka', 'zakazat-zvonok-click');">
                        Заказать звонок
                    </div>
                    <div class="logo">
                        <a href="/">
                            <img src="/images/logo.png" alt="АСД Финанс"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="navbar-header">
        <div class="logo"><img src="/images/logo_white.png" alt="" title=""/></div>
        <div class="phone">
            <?php
            $phone = Option::get('phones_header_phone');
            ?>
            <a href="tel:<?= preg_replace('[^0-9\+]', '', $phone) ?>" class="cpc-tel">
                <?= $phone ?>
            </a>
        </div>
    </div>

    <div class="collapse navbar-collapse">
        <div class="container">
            <ul class="nav navbar-nav">
                <?php if (!$is_home) : ?>
                    <li>
                        <a href="/">
                            Главная
                        </a>
                    </li>
                <?php endif; ?>
                <li>
                    <a href="#slide-1" onclick="return anchorScroller(this)">
                        Калькулятор
                    </a>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        Займы под залог <img src="/images/arrow.png">
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/zajmy-pod-zalog-kvartiry">Квартиры</a></li>
                        <li><a href="/zajmy-pod-zalog-chastnogo-doma">Частного дома</a></li>
                        <li><a href="/zajmy-pod-zalog-kommercheskoj-nedvizhimosti">Коммерческой недвижимости</a>
                        </li>
                        <li><a href="/zajmy-pod-zalog-zemelnogo-uchastka">Земельного участка</a></li>
                        <li><a href="/chastnyj-zajm-pod-zalog">Частный займ</a></li>
                    </ul>
                </li>
                <?php if ($is_home) : ?>
                    <li>
                        <a href="#slide-3"
                           onclick="yaCounter30721508.reachGoal ('predlojenia');return anchorScroller(this)">
                            Наши предложения
                        </a>
                    </li>
                <?php endif; ?>
                <li>
                    <a href="#slide-4"
                       onclick="yaCounter30721508.reachGoal ('preimuchestva');return anchorScroller(this)">
                        Преимущества
                    </a>
                </li>
                <li>
                    <a href="#slide-5" onclick="yaCounter30721508.reachGoal ('vopros');return anchorScroller(this)">
                        Вопросы и ответы
                    </a>
                </li>
                <li>
                    <a href="#slide-6" onclick="yaCounter30721508.reachGoal ('about');return anchorScroller(this)">
                        О компании
                    </a>
                </li>
                <li>
                    <a href="#slide-7" onclick="yaCounter30721508.reachGoal ('contack');return anchorScroller(this)">
                        Контакты
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <?php
    if (extension_loaded('newrelic')) {
        echo newrelic_get_browser_timing_header();
    }
    $this->beginBody();
    ?>

    <main>

        <?= $content ?>

    </main>

    <?php
    $this->endBody();
    if (extension_loaded('newrelic')) {
        echo newrelic_get_browser_timing_footer();
    }
    ?>

    <section id="index-page-map" class="map-holder"></section>

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="block" <?= ($subdomain->domain != 'rostov-na-donu') ? 'style="display: none;"' : '' ?>>
                        <h2 class="map-links__title footer-title">Адрес офиса</h2>
                        <ul class="map-links_list">
                            <li>
                                <a
                                        data-placemark-index="0"
                                        data-x="<?= $subdomain->params['coords'][0] ?>"
                                        data-y="<?= $subdomain->params['coords'][1] ?>"
                                        class="js-map-link"
                                >
                                    <?= $subdomain->params['address'] ?>
                                </a>
                            </li>
                            <!--                        <li><a data-placemark-index="1" data-x="45.03510780" data-y="41.96431350" class="js-map-link">355002, г. Ставрополь, ул. Пушкина, 40</a></li>-->
                            <!--                        <li><a data-placemark-index="2" data-x="45.02662280" data-y="38.97357950" class="js-map-link">350000, г. Краснодар, ул. Красноармейская, дом 60/4</a></li>-->
                            <!--                        <li><a data-placemark-index="3" data-x="48.51451130" data-y="44.54714050" class="js-map-link">400113, г. Волгоград, пр-кт Героев Сталинграда, д. 33</a></li>-->
                            <!--                        <li><a data-placemark-index="4" data-x="48.70749530" data-y="44.51854750" class="js-map-link">400131, г. Волгоград, пр-т Ленина, 16к</a></li>-->
                            <!--                        <li><a data-placemark-index="5" data-x="48.78801870" data-y="44.74999830" class="js-map-link">404111, г. Волжский, ул. Ленина, дом 84 г</a></li>-->
                        </ul>
                    </div>
                    <?php
                    echo RegionsSelect::widget([
                        'subdomains' => \Yii::$app->subdomains,
                        'view_path' => '@frontend/widgets/regions_select/views/regions-list-footer',
                    ]);
                    ?>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="footer_form_bg">
                        <?php
                        $call_order = new CallOrderForm();
                        $call_order_form = ActiveForm::begin([
                            'action' => Url::to(['/site/order-call']),
                            'options' => ['class' => ''],
                            'enableClientValidation' => false,
                        ]);
                        ?>
                        <div class="title">У вас остались вопросы?</div>
                        <div class="text">Оставьте свои данные и наш специалист перезвонит вам в ближайшее время</div>

                        <div class="f_input">
                            <?php
                            echo $call_order_form
                                ->field($call_order, 'name')
                                ->label(false)
                                ->textInput([
                                    'class' => 'form_elem',
                                    'placeholder' => 'Ваше имя',
                                ]);
                            ?>
                        </div>
                        <div class="f_input">
                            <?php
                            echo $call_order_form
                                ->field($call_order, 'phone')
                                ->label(false)
                                ->widget(MaskedInput::className(), [
                                    'class' => 'form_elem',
                                    'mask' => '+7 (999) 999-99-99',
                                    'options' => [
                                        'placeholder' => 'Ваш телефон',
                                        'id' => 'callorderform-phone-2',
                                    ],
                                    'clientOptions' => [
                                        'showMaskOnHover' => false,
                                    ],
                                ]);
                            ?>
                        </div>
                        <div class="f_input">
                            <input type="submit" name="mail_sub" value="Отправить" class="footer-send-button">
                        </div>
                        <?php $call_order_form->end(); ?>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="footer_cont">
                        <div class="phone">
                            <span><a href="tel:88005504109">8 (800) 550 41 09</a></span>
                            <br/>Звонок по России бесплатный
                        </div>
                        <div class="time">
                            График работы горячей линии с 6<sup>00</sup> до 21<sup>00</sup>
                            <br/>
                            Email: <a href="mailto:info@zajmy.su">info@zajmy.su</a>
                        </div>
                    </div>
                    <div class="copira">© <?= date('Y') ?> Общество с ограниченной ответственностью &laquo;МИКРОКРЕДИТНАЯ КОМПАНИЯ АСД-ФИНАНС&raquo;</div>
                </div>
            </div>
        </div>
    </footer>

    <script type="text/javascript"
            src="https://perezvoni.com/files/widgets/4886-cb4c7f8f2cf8345a2364-d5c769cb4c7f8f2-2364-90e5f-c7.js"
            charset="UTF-8"></script>
    </body>
    </html>
<?php
$this->endPage();
