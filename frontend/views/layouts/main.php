<?php
use yii\helpers\Json;

/* @var $this \yii\web\View */
/* @var $content string */

//use backend\modules\content\components\widgets\DownloadablesList;
use common\models\Option;

$subdomain = Yii::$app->subdomains->getCurrent();

$options = [
    'center' => $subdomain->params['coords'],
    'zoom' => $subdomain->params['map_zoom'],
    'placemarks' => [
        [
            'coords' => $subdomain->params['coords'],
            'number' => 0,
            'title' => $subdomain->params['address'],
        ],
    ],
];

?>

<?php $this->beginContent('@app/views/layouts/base.php'); ?>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 blog-main">
                    <div class="slide"></div>
                    <?= $this->render('@app/views/site/_draggers'); ?>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="info phrases-block">
            <div class="container">
                <div class="slide" id="slide-2">&nbsp;</div>
                <div class="col4" data-animated="bounceIn">
                    <div class="num">Сумма займа</div>
                    <span class="num">от</span>
                    <?= number_format(Option::get('draggers_sum_from'), 0, ',', ' ') ?>&nbsp;
                    <span class="rub">a</span>
                    <br>
                    <span class="num">до</span>
                    <?= number_format(Option::get('draggers_sum_to'), 0, ',', ' ') ?>&nbsp;
                    <span class="rub">a</span>
                </div>
                <div class="col4" data-animated="bounceIn">
                    <div class="num">Срок займа</div>
                    <span class="num">до</span> 24 мес **
                </div>
                <div class="col4" data-animated="bounceIn">
                    <div class="num">Регистрация</div>
                    обременения в юстиции
                </div>
                <div class="col4" data-animated="bounceIn">
                    <div class="num">Рассмотрение заявки</div>
                    <span class="num">за</span> 1 день
                </div>
            </div>
        </div>
    </section>

<?= $content; ?>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="slide" id="slide-5"></div>
                    <div class="h2"><h2>Часто задаваемые вопросы</h2></div>
                    <div class="ac-container">

                        <div class="row">
                            <div class="col-lg-6 col-xs-12">
                                <div>
                                    <input id="ac-11" name="accordion-11" type="checkbox"/>
                                    <label for="ac-11">Смотрим ли кредитную историю?</label>
                                    <section class="ac-small">
                                        <p>Кредитная история не имеет значения.</p>
                                    </section>
                                </div>
                                <div>
                                    <input id="ac-12" name="accordion-12" type="checkbox"/>
                                    <label for="ac-12">Принимаются ли в залог комнаты в общежитиях?</label>
                                    <section class="ac-small">
                                        <p>Комнаты в общежитиях не рассматриваются нашей организацией в качестве
                                            обеспечения.</p>
                                    </section>
                                </div>
                                <div>
                                    <input id="ac-13" name="accordion-13" type="checkbox"/>
                                    <label for="ac-13">Принимается ли в залог уже заложенное имущество?</label>
                                    <section class="ac-small">
                                        <p>В залог не принимается недвижимое имущество, имеющее ограничения либо
                                            обременения.</p>
                                    </section>
                                </div>
                                <div>
                                    <input id="ac-14" name="accordion-14" type="checkbox"/>
                                    <label for="ac-14">Принимаются ли в залог доли в общей собственности?</label>
                                    <section class="ac-small">
                                        <p>Залог недвижимости, находящийся в долевой собственности, возможен только в
                                            том
                                            случае, если все собственники долей ( в совокупности составляющие 100% )
                                            выступят
                                            залогодателями.</p>
                                    </section>
                                </div>
                                <div>
                                    <input id="ac-15" name="accordion-15" type="checkbox"/>
                                    <label for="ac-15">Можно ли оформить заем на срок больше года?</label>
                                    <section class="ac-small">
                                        <p>Договор займа может заключаться на 2 года**</p>
                                    </section>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xs-12">
                                <div>
                                    <input id="ac-16" name="accordion-16" type="checkbox"/>
                                    <label for="ac-16">Можно ли оформить заем под материнский капитал?</label>
                                    <section class="ac-small">
                                        <p>Наша организация выдает займы только под залог недвижимого имущества.</p>
                                    </section>
                                </div>
                                <div>
                                    <input id="ac-18" name="accordion-18" type="checkbox"/>
                                    <label for="ac-18">Нужно ли выписываться?</label>
                                    <section class="ac-small">
                                        <p>Выписка жильцов не требуется.</p>
                                    </section>
                                </div>
                                <div>
                                    <input id="ac-19" name="accordion-19" type="checkbox"/>
                                    <label for="ac-19">Можно ли погасить досрочно?</label>
                                    <section class="ac-small">
                                        <p>Досрочное погашение возможно при уведомлении компании.</p>
                                    </section>
                                </div>
                                <div>
                                    <input id="ac-20" name="accordion-20" type="checkbox"/>
                                    <label for="ac-20">Кто проводит оценку?</label>
                                    <section class="ac-small">
                                        <p>Оценка объекта недвижимости проводится силами организации либо с привлечением
                                            аккредитованных оценочных организаций.</p>
                                    </section>
                                </div>
                                <div>
                                    <input id="ac-17" name="accordion-17" type="checkbox"/>
                                    <label for="ac-17">Можно ли оформить заем под залог недвижимости, если один из
                                        собственников
                                        недвижимости несовершеннолетнее лицо?</label>
                                    <section class="ac-small">
                                        <p>В залог не принимается недвижимое имущество, находящееся в собственности
                                            несовершеннолетнего лица.</p>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="money">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title title--no-line">Нужны деньги, а банки отказывают?</h2>
                    <p class="description" data-animated="fadeInLeft">Получите заем под залог недвижимости. Рассмотрение
                        за 1-3 дня.<br/>Без подтверждения дохода, с любой кредитной историей.</p>
                    <div
                        class="zayavka zayavka_right"
                        data-animated="fadeInRight"
                        onclick="yaCounter30721508.reachGoal('zaivka-click'); ga('send', 'event', 'Knopka', 'zaivka-click');"
                    >
                        Оформить заявку
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="slide" id="slide-6"></div>
                    <div class="h2"><h2>О компании</h2></div>

                    <p>Согласно приказу ЦБ РФ № ОД-194 от 29.01.2018г. сведения об Обществе с ограниченной ответственностью «МИКРОКРЕДИТНАЯ КОМПАНИЯ АСД-ФИНАНС» исключены из государственного реестра микрофинансовых организаций.</p>

                    <div class="clr"></div>
                    <div class="doc-4">
                        <ul class="pdf">
                            <li><a href="/downloads/Приказ_об_исключении_сведений.pdf" target="_blank">Приказ об исключении сведений ООО «МКК АСД-Финанс» из гос. реестра МФО</a></li>
                        </ul>
                    </div>
                    <div class="doc-4">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="slide" id="slide-7"></div>
                    <div class="h2"><h2>Контакты</h2></div>
                    <?php if ($subdomain->domain != 'rostov-na-donu') { ?>
                        <div class="contacts-explanation">
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="contacts-explanation__step">
                                        <div class="contacts-explanation__digit">1</div>
                                        <p class="contacts-explanation__for">
                                            Для получения займа под залог недвижимости
                                        </p>
                                        <p class="contacts-explanation__do">
                                            в <?= $subdomain->params['prepositional_region'] ?>
                                            заполните онлайн заявку и наши специалисты свяжутся с Вами.
                                            Решение по заявке принимается в удаленном режиме и в кратчайшие сроки.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="contacts-explanation__step">
                                        <div class="contacts-explanation__digit">
                                            2
                                        </div>
                                        <p class="contacts-explanation__for">
                                            Для подтверждения права собственности
                                        </p>
                                        <p class="contacts-explanation__do">
                                            необходимо будет предоставить скан-копии оригиналов документов на залоговою
                                            недвижимость.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="contacts-explanation__step">
                                        <div class="contacts-explanation__digit">
                                            3
                                        </div>
                                        <p class="contacts-explanation__for">
                                            Для подписания договора займа
                                        </p>
                                        <p class="contacts-explanation__do">
                                            наш специалист приедет по адресу нахождения залога.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="map_phone">
                        <a href="http://maps.apple.com/maps?q=47.20394780,39.67697850">
                            <span>Посмотреть адрес на карте</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <input type="hidden" name="map-data" value='<?= Json::encode($options) ?>'/>
                </div>
            </div>
        </div>
    </section>

<?php $this->endContent(); ?>