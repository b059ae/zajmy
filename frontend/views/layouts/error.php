<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Json;

$subdomain = Yii::$app->subdomains->getCurrent();

$options = [
    'center' => $subdomain->params['coords'],
    'zoom' => $subdomain->params['map_zoom'],
    'placemarks' => [
        [
            'coords' => $subdomain->params['coords'],
            'number' => 0,
            'title' => $subdomain->params['address'],
        ],
    ],
];

?>

<?php $this->beginContent('@app/views/layouts/base.php'); ?>

<?= $content; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="slide" id="slide-7"></div>
                <div class="h2"><h2>Контакты</h2></div>
                <?php if ($subdomain->domain != 'rostov-na-donu') { ?>
                    <div class="contacts-explanation">
                        <div class="row">
                            <div class="col-md-4 col-xs-12">
                                <div class="contacts-explanation__step">
                                    <div class="contacts-explanation__digit">1</div>
                                    <p class="contacts-explanation__for">
                                        Для получения займа под залог недвижимости
                                    </p>
                                    <p class="contacts-explanation__do">
                                        в <?= $subdomain->params['prepositional_region'] ?>
                                        заполните онлайн заявку и наши специалисты свяжутся с Вами.
                                        Решение по заявке принимается в удаленном режиме и в кратчайшие сроки.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="contacts-explanation__step">
                                    <div class="contacts-explanation__digit">
                                        2
                                    </div>
                                    <p class="contacts-explanation__for">
                                        Для подтверждения права собственности
                                    </p>
                                    <p class="contacts-explanation__do">
                                        необходимо будет предоставить скан-копии оригиналов документов на залоговою
                                        недвижимость.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4 col-xs-12">
                                <div class="contacts-explanation__step">
                                    <div class="contacts-explanation__digit">
                                        3
                                    </div>
                                    <p class="contacts-explanation__for">
                                        Для подписания договора займа
                                    </p>
                                    <p class="contacts-explanation__do">
                                        наш специалист приедет по адресу нахождения залога.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div class="map_phone">
                    <a href="http://maps.apple.com/maps?q=47.20394780,39.67697850">
                        <span>Посмотреть адрес на карте</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <input type="hidden" name="map-data" value='<?= Json::encode($options); ?>'/>
            </div>
        </div>
    </div>
</section>

<?php $this->endContent(); ?>