<?php
return [
    'emails_call_order_email' => [
        'name' => 'E-Mail форм отпавки, куда отправлять',
        'value' => 'info@zajmy.su'
    ],
    //
    'draggers_service_active' => [
        'name' => 'Сервис включен',
        'value' => true
    ],
    'draggers_interest_per_month' => [
        'name' => 'Процент в месяц. В виде числа <1',
        'value' => 0.04
    ],
    'draggers_interest_per_month_fixed' => [
        'name' => 'Фиксированый процент в месяц. В виде числа <1',
        'value' => 0.0295
    ],
    'draggers_interest_per_month_annuity' => [
        'name' => 'Аннуитетный процент в месяц. В виде числа <1',
        'value' => 0.05
    ],
    'draggers_sum_from' => [
        'name' => 'Сумма - от',
        'value' => 300000
    ],
    'draggers_sum_to' => [
        'name' => 'Сумма - до',
        'value' => 10000000
    ],
    'draggers_sum_step' => [
        'name' => 'Сумма - длина шага',
        'value' => 50000
    ],
    'draggers_sum_default' => [
        'name' => 'Сумма по-умолчанию',
        'value' => 1000000
    ],
    'draggers_term_from' => [
        'name' => 'Срок - от',
        'value' => 12
    ],
    'draggers_term_to' => [
        'name' => 'Срок - до',
        'value' => 24
    ],
    /*'draggers_term_step' => [
        'name' => 'Срок - шаг',
        'value' => 1
    ],*/
    'draggers_term_default' => [
        'name' => 'Срок по-умолчанию',
        'value' => 12
    ],
    'phones_header_phone' => [
        'name' => 'Телефон в шапке',
        'value' => '8 (800) 550 41 09'
    ],
    'phones_footer_phone' => [
        'name' => 'Телефон в подвале',
        'value' => '8 (800) 550 41 09'
    ],
    //
    'call_order_service_active' => [
        'name' => 'Включена ли услуга',
        'value' => 1
    ],
    'call_order_service_id' => [
        'name' => 'Идентификатор услуги для колцентра',
        'value' => 87
    ],
    'call_order_service_url' => [
        'name' => 'Ссылка отправки заявки',
        'value' => 'http://callcenter.vpn.dengisrazy.ru/admin/autodial/call.php'
    ],
    'call_order_schedule_form_begin' => [
        'name' => 'Начало показа кнопки услуги',
        'value' => '00:00:00'
    ],
    'call_order_schedule_form_end' => [
        'name' => 'Конец показа кнопки услуги',
        'value' => '23:59:59'
    ],
    'call_order_schedule_call_begin' => [
        'name' => 'Начало работы колцентра',
        'value' => '06:00'
    ],
    'call_order_schedule_call_end' => [
        'name' => 'Конец работы колцентра',
        'value' => '21:00'
    ]
];