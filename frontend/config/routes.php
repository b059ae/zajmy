<?php

return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,

    'rules' => [
        '/' => '/site/index',
        '/sitemap.xml' => '/site/sitemap',
        '/robots.txt' => '/site/robots',
        '/<slug:(\w|-)*>' => '/site/page',
    ]
];
