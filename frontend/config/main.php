<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'zajmy.su',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute' => 'site/index',
    'components' => [
        'subdomains' => [
            'class' => 'common\components\subdomains\Subdomains',
            'provider' => require(__DIR__ . '/subdomains.php')
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'bundles' => [
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [],
                    'css' => [],
                ],
                'yii\web\JqueryAsset' => [
                    'js' => ['https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js']
                ],
            ],
        ],
        'urlManager' => require(__DIR__ . '/routes.php'),
    ],
    'aliases' => [
        '@asdfinans' => dirname(__DIR__),
    ],
    'params' => $params
];
