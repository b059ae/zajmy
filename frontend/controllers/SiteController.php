<?php

namespace frontend\controllers;

use common\components\sitemap\SiteMapAction;
use common\models\ApplicationForm;
use common\models\CallOrderForm;
use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'sitemap' => [
                'class' => SiteMapAction::className(),
                'subdomains' => Yii::$app->subdomains,
                'sites_maps_path' => '/sitemaps/zajmy.su',
                'file_name' => 'sitemap.xml',
            ],
            'robots' => [
                'class' => SiteMapAction::className(),
                'subdomains' => Yii::$app->subdomains,
                'sites_maps_path' => '/sitemaps/zajmy.su',
                'file_name' => 'robots.txt',
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPage($slug)
    {
        return $this->render('pages/'.$slug);
    }

    /**
     * @return array|bool
     */
    public function actionOrderCall()
    {
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;
        $model = new CallOrderForm;

        if ($model->load(Yii::$app->getRequest()->post()) && $model->validate()) {
            if (!$model->save()) {
                return ['error' => ['time' => ["Произошла неизвестная ошибка.\nПриносим свои извинения."]]];
            }
        } else {
            return ['error' => ['time' => implode(', ', $model->getFirstErrors())]];
        }

        return true;
    }

    /**
     * @return array
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function actionApply()
    {
        Yii::$app->response->format = 'json';

        $model = new ApplicationForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (!$model->save()) {
                return ['error' => "Произошла неизвестная ошибка.\nПриносим свои извинения."];
            }
        } else {
            return [
                'error' => $model->hasErrors() ? implode(', ', $model->getFirstErrors()) : '',
            ];
        }

        return true;
    }
}
