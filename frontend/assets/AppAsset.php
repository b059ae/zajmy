<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class AppAsset
 * @package frontend\assets
 */
class AppAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@frontend/views/assets';

    /**
     * @inheritdoc
     */
    public $css = [
        'css/bootstrap.css',
        'css/style.css?20180217',
        'css/animate.css',
        'css/highslide.css',
    ];

    /**
     * @inheritdoc
     */
    public $js = [
        'js/bootstrap.min.js',
        'js/callNW.js',
        'js/modernizr.custom.29473.js',
        'js/jquery-ui-1.10.3.custom.min.js',
        'js/javascript.js',
        'js/ui.js',
        'js/jquery.nicescroll.js',
        'https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU',
        'js/map-init.js',
        'js/highslide-with-gallery.js',
        'js/call-highslide.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\YiiAsset',
        'frontend\assets\DragdealerAsset',
        'frontend\assets\AutoNumericAsset',
        'frontend\assets\LoanDraggersAsset',
        'common\assets\UniformAsset',
        'common\assets\UnderscoreAsset',
    ];
}
