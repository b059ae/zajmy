<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class AutoNumericAsset
 * @author German Sokolov
 * @package frontends\asdfinans\assets
 */
class AutoNumericAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@frontend/views/assets';

    /**
     * @inheritdoc
     */
    public $js = [
        'js/autoNumeric.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
