<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Готовые бегунки калькулятора ПСК
 * Class LoanDraggersAsset
 * @author German Sokolov
 */
class LoanDraggersAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@frontend/views/assets';

    /**
     * @inheritdoc
     */
    public $js = [
        'js/number_format.js',
        'js/loan-draggers.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\YiiAsset',
        'frontend\assets\DragdealerAsset',
        'common\assets\UniformAsset',
        'common\assets\UnderscoreAsset',
        'frontend\assets\AutoNumericAsset',
    ];
}
