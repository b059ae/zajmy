<?php

/**
 * @var Subdomain[] $subdomains
 * @var Subdomain $current_subdomain
 * @var View $this
 */

use yii\web\View;
use common\components\subdomains\Subdomain;

?>

<div class="regions-list-mobile js-regions-list-mobile">
    <span class="regions-list-mobile__current"><?= $current_subdomain->name ?></span>
    <div class="regions-list-mobile__list-block">
        <ul class="regions-list-mobile__list">
            <?php foreach ($subdomains as $subdomain) { ?>
                <li class="regions-list-mobile__item <?= $subdomain->is_current ? 'regions-list-mobile__item--selected' : '' ?>">
                    <?php if ($subdomain->is_current) { ?>
                        <?= $subdomain->name ?>
                    <?php } else { ?>
                        <a class="regions-list-mobile__link" href="<?= $subdomain->url ?>">
                            <?= $subdomain->name ?>
                        </a>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>