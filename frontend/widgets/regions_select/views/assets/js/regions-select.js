"use strict";

var RegionsSelect;

(function () {

    RegionsSelect = function () {
        this.init();
    };

    RegionsSelect.prototype.init = function (options) {
        $('.js-regions-select').change(function (event) {
            var url = $(event.currentTarget).val();
            window.location.href = url;
        });

        var $list_mobile = $('.js-regions-list-mobile');
        $list_mobile.click(function () {
            $list_mobile.toggleClass('active');
        });
    };

})();
