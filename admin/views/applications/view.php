<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Applications */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Applications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="applications-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Удалить заявку?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'phone',
            'name',
            'domain',
            'note',
            [
                'attribute' => 'subject_type',
                'value' => function (\common\models\Applications $model) {
                    return $model->getSubjectTypeName();
                }
            ],
            'city',
            'sum',
            'description',
            [
                'attribute' => 'payments_type',
                'value' => function (\common\models\Applications $model) {
                    return $model->getPaymentsTypeName();
                }
            ],
            'created_at:datetime',
        ],
    ]) ?>

</div>
