<?php

namespace common\components\sitemap;

/**
 * Class SiteMapHTTPRequest
 *
 * @package common\components\sitemap
 */
class SiteMapHTTPRequest extends \PHPCrawlerHTTPRequest
{
    /**
     * @inheritdoc
     */
    protected function openSocket(&$error_code, &$error_string)
    {
        \PHPCrawlerBenchmark::reset("connecting_server");
        \PHPCrawlerBenchmark::start("connecting_server");

        if ($this->url_parts["protocol"] == "https://") {
            $protocol_prefix = "ssl://";
        } else {
            $protocol_prefix = "";
        }

        if ($protocol_prefix == "ssl://" && !extension_loaded("openssl")) {
            $error_code = \PHPCrawlerRequestErrors::ERROR_SSL_NOT_SUPPORTED;
            $error_string = "Error connecting to " . $this->url_parts["protocol"] . $this->url_parts["host"] . ": SSL/HTTPS-requests not supported, extension openssl not installed.";
        }

        $ip_address = $this->DNSCache->getIP($this->url_parts["host"]);

        if ($this->proxy != null) {
            $this->socket = @stream_socket_client(
                $this->proxy["proxy_host"] . ":" . $this->proxy["proxy_port"],
                $error_code,
                $error_str,
                $this->socketConnectTimeout,
                STREAM_CLIENT_CONNECT
            );
        } else {
            if ($this->url_parts["protocol"] == "https://") {
                $context = stream_context_create([
                    'ssl' => [
                        'peer_name' => $this->url_parts["host"],
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ]);
                $this->socket = stream_socket_client(
                    $protocol_prefix . $ip_address . ":" . $this->url_parts["port"],
                    $error_code,
                    $error_str,
                    $this->socketConnectTimeout,
                    STREAM_CLIENT_CONNECT,
                    $context
                );
            } else {
                $this->socket = @stream_socket_client(
                    $protocol_prefix . $ip_address . ":" . $this->url_parts["port"],
                    $error_code,
                    $error_str,
                    $this->socketConnectTimeout,
                    STREAM_CLIENT_CONNECT
                );
            }
        }

        $this->server_connect_time = \PHPCrawlerBenchmark::stop("connecting_server");

        if ($this->socket == false) {
            $this->server_connect_time = null;

            if ($this->proxy != null) {
                $error_code = \PHPCrawlerRequestErrors::ERROR_PROXY_UNREACHABLE;
                $error_string = "Error connecting to proxy " . $this->proxy["proxy_host"] . ": Host unreachable (" . $error_str . ").";

                return false;
            } else {
                $error_code = \PHPCrawlerRequestErrors::ERROR_HOST_UNREACHABLE;
                $error_string = "Error connecting to " . $this->url_parts["protocol"] . $this->url_parts["host"] . ": Host unreachable (" . $error_str . ").";

                return false;
            }
        } else {
            return true;
        }
    }
}
