<?php

namespace common\components\helpers;

/**
 * Вспомогательные хелперы строк
 * Class StringHelper
 * @author German Sokolov
 * @package common\components\helpers
 */
class StringHelper extends \yii\helpers\StringHelper
{

    /**
     * Выбор формы для множестенного числа
     * @param int $num - число
     * @param string[] $forms - формы
     * @return string
     * @example echo StringHelper::pluralize(42, ['карандаш', 'карандаша', 'карандашей']);
     */
    public static function pluralize($num, $forms)
    {
        if ($num >= 11 && $num <= 19) {
            return $forms[2];
        }

        $digit = $num % 10;
        if ($digit == 1) {
            return $forms[0];
        }
        if ($digit >= 2 && $digit <= 4) {
            return $forms[1];
        }

        return $forms[2];
    }
}
