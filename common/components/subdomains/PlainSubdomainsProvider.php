<?php

namespace common\components\subdomains;

use yii\base\Component;

/**
 * Class Subdomains
 * @author German Sokolov
 * @package common\components
 */
class PlainSubdomainsProvider extends Component implements SubdomainsProviderInterface
{
    /**
     * @var array
     */
    public $regions = [];

    /**
     * @var string|null
     */
    public $default = null;

    /**
     * @return array
     */
    public function getList()
    {
        return $this->regions;
    }

    /**
     * @return string|null
     */
    public function getDefaultSubdomain()
    {
        return $this->default;
    }
}
