<?php

namespace common\components\subdomains;

/**
 * Class Subdomains
 * @author German Sokolov
 * @package common\components
 */
interface SubdomainsProviderInterface
{
    /**
     * @return array - словарь 'domain_value' => ['name' => 'name_value', ...], ...
     */
    public function getList();

    /**
     * @return string
     */
    public function getDefaultSubdomain();
}
