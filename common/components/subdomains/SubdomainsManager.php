<?php

namespace frontend\components\subdomains;

use backend\modules\company\modules\department\models\department_types\DepartmentLending;
use backend\modules\service\modules\cities_vocabulary\models\CityVocabulary;
use common\components\ActiveQuery;
use common\components\Query;
use yii\base\Component;
use yii\db\Expression;
use yii\helpers\Url;

class SubdomainsManager extends Component
{
    /**
     * Ключ переменной $_SERVER, в которой доменное имя
     * @var string
     */
    public $domain_server_var = 'HTTP_HOST';

    /**
     * @var Region[]
     */
    private $regions;

    /**
     * @var City|null
     */
    private $current_city;

    /**
     * @var bool
     */
    private $current_city_cached = false;

    /**
     * Получить город текущего поддомена, если есть поддомен и город найден. Иначе - null
     * @return City|null
     */
    public function getCurrent()
    {
        if (!$this->current_city_cached) {
            $this->current_city = $this->getCurrentWithoutCache();
            $this->current_city_cached = true;
        }

        return $this->current_city;
    }

    /**
     * Список всех регионов, отсортированный
     * @return Region[]
     */
    public function getRegions()
    {
        if (!isset($this->regions)) {
            $this->regions = $this->getRegionsFromQuery(CityVocabulary::find());
        }

        return $this->regions;
    }

    /**
     * Список всех городов региона, отсортированный
     * @param string $region_name - кириллическое название региона (region_ru)
     * @return City[]
     */
    public function getCitiesByRegionName(string $region_name)
    {
        return $this->getCitiesFromQuery(CityVocabulary::find()->andWhere(['region_ru' => $region_name]));
    }

    /**
     * @param string $name
     * @return City|null
     */
    public function getCityByEnName($name)
    {
        $cities = $this->getCitiesFromQuery(CityVocabulary::find()
            ->andWhere(['city_en' => $name])
        );

        return $cities ? $cities[0] : null;
    }

    /**
     * Получить урл поддомена в контексте определенного роута
     * @param City $city
     * @param array|string $route
     * @return string
     */
    public function getUrlTo(City $city, $route)
    {
        $domain = $_SERVER[$this->domain_server_var];
        $parts = explode('.', $domain);
        if (count($parts) > 2) {
            array_shift($parts);
        }
        $subdomain = $city->subdomain;
        if ($subdomain) {
            array_unshift($parts, $subdomain);
        }
        $new_domain = implode('.', $parts);

        return '//' . $new_domain . Url::toRoute($route);
    }

    /**
     * Фраза " в <название-города>"
     * @return string|null
     */
    public function phraseIn()
    {
        return $this->getCurrent()
            ? $this->getCurrent()->phraseIn()
            : null;
    }

    /**
     * @return City|null
     */
    private function getCurrentWithoutCache()
    {
        $subdomain = $this->getCurrentSubdomain();
        if (!$subdomain) {
            return null;
        }
        $records = $this->getCitiesFromQuery(CityVocabulary::find()
            ->andWhere(['city_en' => $subdomain])
        );

        return $records ? $records[0] : null;
    }

    /**
     * @param ActiveQuery|Query $query
     * @return Region[]
     */
    private function getRegionsFromQuery($query)
    {
        $records = $query
            ->andWhere(['region_ru' => $this->getAllDepartment('region')])
            ->select([
                'name'=>'region_ru',
                'prepositional_case' => 'region_inf',
            ])
            ->orderBy(new Expression('REPLACE(name, "Республика ", "")'))
            ->distinct()
            ->asArray()
            ->all();

        $list = [];
        foreach ($records as $record) {
            $list[] = new Region([
                'name' => $record['name'],
                'prepositional_case' => $record['prepositional_case'],
            ]);
        }

        return $list;
    }

    /**
     * @param ActiveQuery|Query $query
     * @return City[]
     */
    private function getCitiesFromQuery($query)
    {
        $records = $query->andWhere(['city_ru' => $this->getAllDepartment('city')])
            ->select([
                'subdomain' => 'city_en',
                'name' => 'city_ru',
                'prepositional_case' => 'city_inf',
                'region_name' => 'region_ru',
                'region_prepositional_case' => 'region_inf',
            ])
            ->orderBy(['name' => SORT_ASC])
            ->distinct()
            ->asArray()
            ->all();

        $list = [];
        foreach ($records as $record) {
            $list[] = new City([
                'subdomain' => $record['subdomain'],
                'name' => $record['name'],
                'prepositional_case' => $record['prepositional_case'],
                'region' => new Region([
                    'name' => $record['region_name'],
                    'prepositional_case' => $record['region_prepositional_case'],
                ])
            ]);
        }

        return $list;
    }

    /**
     * Получить все города или регионы, в которых есть ЦВЗ
     * @param string $col
     * @return string[]
     */
    private function getAllDepartment($col)
    {
        return DepartmentLending::find()
            ->andWhere([
                'type' => DepartmentLending::getType(),
                'status' => DepartmentLending::STATUS_ACTIVE,
            ])
            ->orderBy([$col => SORT_ASC])
            ->distinct()
            ->select($col)
            ->column();
    }

    /**
     * @return string|null
     */
    private function getCurrentSubdomain()
    {
        $domain = $_SERVER[$this->domain_server_var];
        $parts = explode('.', $domain);
        if (count($parts) > 2) {
            return $parts[0];
        } else {
            return null;
        }
    }
}
