<?php

namespace common\components\subdomains;

use yii\helpers\ArrayHelper;
use yii\base\Component;
use yii\helpers\Url;

/**
 * Class Subdomains
 * @author German Sokolov
 * @package common\components
 */
class Subdomains extends Component
{
    /**
     * @var string
     */
    public $domain_server_var = 'HTTP_HOST';

    /**
     * @var SubdomainsProviderInterface
     */
    public $provider;

    /**
     * @var Subdomain|null
     */
    private $current;

    /**
     * @var array|null
     */
    private $pattern_route = null;

    //Флаги заполненности кеша должны быть отдельно, так как должен мочь успешно кешироваться null

    /**
     * @var bool
     */
    private $current_cached = false;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        if (is_array($this->provider)) {
            $this->provider = \Yii::createObject($this->provider);
        }
    }

    public static function assocMap(array $array, callable $callback, $preserve_keys = false)
    {
        $result = [];
        foreach ($array as $key => $value) {
            if ($preserve_keys) {
                $result[$key] = $callback($key, $value);
            } else {
                $result[] = $callback($key, $value);
            }
        }

        return $result;
    }

    /**
     * @param bool $use_default - получать ли дефолтный вместо nul
     * @return string|null
     */
    public function getSubdomain($use_default = true)
    {
        $domain = $_SERVER[$this->domain_server_var];
        $parts = explode('.', $domain);
        if (count($parts) > 2) {
            return $parts[0];
        } else {
            return $use_default ? $this->provider->getDefaultSubdomain() : null;
        }
    }

    /**
     * @param string|null $subdomain
     * @return string
     */
    public function getSubdomainUrl($subdomain)
    {
        $domain = $_SERVER[$this->domain_server_var];
        $parts = explode('.', $domain);
        if (count($parts) > 2) {
            array_shift($parts);
        }
        if ($subdomain) {
            array_unshift($parts, $subdomain);
        }
        $new_domain = implode('.', $parts);

        if ($this->pattern_route) {
            $route = $this->pattern_route;
        } else {
            $route = \Yii::$app->request->queryParams;
            $route[0] = '/' . \Yii::$app->controller->route;
        }
        return '//' . $new_domain . Url::to($route);
    }

    /**
     * По дефолту в субдоменах и рут-секшенах есть урл составленный по текущему роуту
     * Можно определить для них другой роут.
     * @param array $route
     * @return static
     */
    public function setPatternRoute(array $route)
    {
        $this->pattern_route = $route;
        //Инвалидировать кеш
        $this->current_cached = false;
        return $this;
    }

    /**
     * @return Subdomain[]
     */
    public function getSubdomains()
    {
        $current_domain = $this->getSubdomain();

        $cache_key = 'subdmns_' . \Yii::$app->id;
        $list = \Yii::$app->cache->getOrSet($cache_key, function () {
            return $this->provider->getList();
        }, 86400);

        return $this->assocMap($list, function ($domain, $config) use ($current_domain) {
            return new Subdomain([
                'domain' => $domain,
                'name' => $config['name'] ?? null,
                'prepositional_case' => $config['prepositional_case'] ?? null,
                'seo_title' => $config['seo_title'] ?? null,
                'seo_desc' => $config['seo_desc'] ?? null,
                'url' => $this->getSubdomainUrl($domain),
                'is_current' => $domain == $current_domain,
                'params' => $config['params'] ?? [],
            ]);
        });
    }

    /**
     * @return Subdomain|null
     */
    public function getCurrent()
    {
        if (!$this->current_cached) {
            $this->current = $this->getCurrentUncached();
            $this->current_cached = true;
        }
        return $this->current;
    }

    /**
     * @return Subdomain|null
     */
    private function getCurrentUncached()
    {
        foreach ($this->getSubdomains() as $subdomain) {
            if ($subdomain->is_current) {
                return $subdomain;
            }
        }
        return null;
    }
}
