<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class UniformAsset
 * @package common\assets
 * @author German Sokolov
 */
class UniformAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower-asset/jquery.uniform';

    public $css = [];

    public $js = [
        'jquery.uniform.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}
