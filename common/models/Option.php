<?php

namespace common\models;

use common\components\helpers\UtilsHelper;
use yii\base\Model;

class Option extends Model
{
    public static function get(string $code)
    {
        if (($option = \Yii::$app->params['options'][$code])) {
            $value = $option['value'];

            if (UtilsHelper::isValueEmpty($value)) {
                return null;
            } else {
                return $value;
            }
        }

        return null;
    }
}
