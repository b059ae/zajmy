<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "applications".
 *
 * @property integer $id
 * @property string $phone
 * @property string $name
 * @property string $note
 * @property string $domain
 * @property integer $subject_type
 * @property string $city
 * @property integer $sum
 * @property string $description
 * @property integer $payments_type
 * @property integer $created_at
 * @property integer $updated_at
 */
class Applications extends \yii\db\ActiveRecord
{
    const INDIVIDUAL = 0;
    const LEGAL_ENTITY = 1;
    const SOLE_PROPRIETOR = 2;

    const PAYMENTS_PERCENT = 0;
    const PAYMENTS_ANNUITY = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'applications';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'name'], 'required'],
            [['subject_type', 'sum', 'payments_type', 'created_at', 'updated_at'], 'integer'],
            [['phone', 'name', 'note', 'city', 'description', 'domain'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Телефон',
            'name' => 'Имя',
            'note' => 'Примечание',
            'domain' => 'Домен',
            'subject_type' => 'Тип субъекта',
            'city' => 'Город',
            'sum' => 'Сумма',
            'description' => 'Описание залога',
            'payments_type' => 'Тип погашения',
            'created_at' => 'Дата',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return array
     * @static
     */
    public static function getSubjectTypesList()
    {
        return [
            self::INDIVIDUAL => 'Физ. лицо',
            self::LEGAL_ENTITY => 'Юр. лицо',
            self::SOLE_PROPRIETOR => 'ИП',
        ];
    }

    /**
     * @return string
     */
    public function getSubjectTypeName(){
        $list = self::getSubjectTypesList();
        return !empty($list[$this->subject_type]) ? $list[$this->subject_type] : $this->subject_type;
    }

    /**
     * @return array
     * @static
     */
    public static function getPaymentsTypesList()
    {
        return [
            self::PAYMENTS_PERCENT => 'Только проценты',
            self::PAYMENTS_ANNUITY => 'Равными частями',
        ];
    }

    /**
     * @return string
     */
    public function getPaymentsTypeName(){
        $list = self::getPaymentsTypesList();
        return !empty($list[$this->payments_type]) ? $list[$this->payments_type] : $this->payments_type;
    }
}
