<?php
return [
    'adminEmail' => 'info@zajmy.su',
    'supportEmail' => 'info@zajmy.su',
    'user.passwordResetTokenExpire' => 3600,
];
