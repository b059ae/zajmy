User-Agent: Yandex
Disallow: /*openstat*
Disallow: /*utm*
Disallow: /*from*
Disallow: /*gclid*
Disallow: /*yclid*

User-Agent: Googlebot
Disallow: /*openstat*
Disallow: /*utm*
Disallow: /*from*
Disallow: /*gclid*
Disallow: /*yclid*

User-Agent: *
Disallow: /*openstat*
Disallow: /*utm*
Disallow: /*from*
Disallow: /*gclid*
Disallow: /*yclid*

Host: https://moscow.zajmy.su
Sitemap: https://moscow.zajmy.su/sitemap.xml

